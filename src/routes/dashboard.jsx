import Chart from "../views/Chart/Chart";

import {
  Dashboard,
  Person,
  ContentPaste,
  LibraryBooks,
  BubbleChart,
  LocationOn,
  Notifications
} from "@material-ui/icons";

const dashboardRoutes = [
  {
    path: "/chart",
    sidebarName: "Chart",
    navbarName: "Chart",
    icon: BubbleChart,
    component: Chart
  },
  { redirect: true, path: "/", to: "/chart", navbarName: "Redirect" }
];

export default dashboardRoutes;
