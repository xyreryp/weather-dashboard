import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
import StartForm from "../../components/Search/StartTime";
import EndForm from "../../components/Search/EndTime";
import TimeAgo from 'react-timeago';
import Locations from '../../components/CustomInput/Location';
import Sensors from '../../components/CustomInput/Sensors';
import Duration from '../../components/Search/TimeDuration';
import Rain from "../../components/Graphs/Rain";
import Wind from "../../components/Graphs/WindDirection";
import SearchLinks from "../../components/Search/SearchLinks";
import Solar from "../../components/Graphs/Solar";
import Dew from "../../components/Graphs/DEW";
import Gust from "../../components/Graphs/Gust";
import Temp from "../../components/Graphs/Temp";
import RH from "../../components/Graphs/RH";

import {
  ContentCopy,
  Store,
  InfoOutline,
  Warning,
  DateRange,
  LocalOffer,
  Update,
  ArrowUpward,
  AccessTime,
  Accessibility
} from "@material-ui/icons";
import Button from '@material-ui/core/Button';
import { withStyles, Grid } from "material-ui";
import Iframe from "react-iframe";

import {
  StatsCard,
  ChartCard,
  TasksCard,
  RegularCard,
  Table,
  ItemGrid
} from "../../components";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "../../variables/charts";

import dashboardStyle from "../../assets/jss/material-dashboard-react/dashboardStyle";

class Chart extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.headerCallback = this.headerCallback.bind(this);
    this.state = {
      config: [],
      headerData: [],
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  headerCallback = dataFromHeader => {
    this.props.callback(dataFromHeader);
  };

  render() {
    return (
      <div>
        <Grid container>
          <Grid item md={12}>
            <SearchLinks
              callback={this.headerCallback}
              selection={this.props.headerData}
            />

          </Grid>
        </Grid>
        <br />
        <Grid container>
          <Grid item md={12}>
            <Rain />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item md={12}>
            <Wind/>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item md={12}>
            {/*<Solar/>*/}
          </Grid>
        </Grid>
        <Grid container>
          <Grid item md={12}>
            <Dew />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item md={12}>
            <Gust />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item md={12}>
            <Temp />
          </Grid>
        </Grid>
        <Grid container>
          <Grid item md={12}>
            <RH />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(dashboardStyle)(Chart);
