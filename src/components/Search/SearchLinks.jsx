import React from "react";
import PropTypes from "prop-types";
import Location from "../CustomInput/Location";
import Sensor from "../CustomInput/Sensors";
import StartTime from "./StartTime";
import EndTime from "./EndTime";
import Interval from "./TimeDuration";

import { Grid } from "material-ui";
import Button from '@material-ui/core/Button';

class SearchLinks extends React.Component{
  constructor(props) {
    super(props);
    // bind the different callbacks and set initial state.
    this.buttonHandler = this.buttonHandler.bind(this);
    this.locationHandler = this.locationHandler.bind(this);
    this.sensorHandler = this.sensorHandler.bind(this);
    this.startTimeHandler = this.startTimeHandler.bind(this);
    this.endTimeHandler = this.endTimeHandler.bind(this);
    this.intervalHandler = this.intervalHandler.bind(this);

    this.state = {
      location: this.props.location,
      sensor: this.props.sensor,
      start: this.props.start,
      end: this.props.end,
      interval: this.props.interval,
    }
  }

  locationHandler(selection) {
    this.setState(
      {
        location: selection,
      }
    )
  }

  sensorHandler(selection) {
    this.setState(
      {
        sensor: selection,
      }
    )
  }

  startTimeHandler(selection) {
    this.setState(
      {
        start: selection,
      }
    )
  }

  endTimeHandler(selection) {
    this.setState(
      {
        end: selection,
      }
    )
  }

  intervalHandler(selection) {
    this.setState(
      {
        interval: selection,
      }
    )
  }

  buttonHandler() {
    this.props.callback(this.state);
  }

  render() {
    return (
      <div>
        <Grid container>
          <Grid item  xs={3} sm={3} md={3}>
          </Grid>
          <Grid item  xs={3} sm={3} md={3}>
            <Location
              selection={this.state}
              callback={this.locationHandler}
            />
          </Grid>
          <Grid item  xs={3} sm={3} md={3}>
            <Sensor
              selection={this.state}
              callback={this.sensorHandler}
            />
          </Grid>
          <Grid item  xs={3} sm={3} md={3}>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item  xs={3} sm={3} md={3}>
            <StartTime
              selection={this.state}
              callback={this.startTimeHandler}
            />
          </Grid>
          <Grid item  xs={3} sm={3} md={3}>
            <EndTime
              selection={this.state}
              callback={this.endTimeHandler}
            />
          </Grid>
          <Grid item xs={3} sm={3} md={3}>
            <Interval
              selection={this.state}
              callback={this.intervalHandler}
            />
          </Grid>
          <Grid item  xs={3} sm={3} md={3} style={{ "padding-top": "10px"}}>
            <Button variant="outlined" color="primary"
                    onClick={this.buttonHandler}
            >
              Apply
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default SearchLinks;