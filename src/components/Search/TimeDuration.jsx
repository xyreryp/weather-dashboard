import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});
class TimeDuration extends React.Component {
  state = {
    duration: ''
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-native-helper">Interval</InputLabel>
          <NativeSelect
            value={this.state.duration}
            onChange={this.handleChange('duration')}
            input={<Input id="age-native-helper" />}
          >
            <option value="" />
            <option value={"5m"}>5 minutes</option>
            <option value={"10m"}>10 minutes</option>
            <option value={"15m"}>15 minutes</option>
            <option value={"30m"}>30 minutes</option>
            <option value={"45m"}>45 minutes</option>
            <option value={"1h"}>1 hour</option>
            <option value={"12h"}>12 hours</option>
            <option value={"1d"}>1 day</option>
            <option value={"1w"}>1 week</option>
          </NativeSelect>
        </FormControl>
      </div>
    );
  }
}

TimeDuration.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TimeDuration);