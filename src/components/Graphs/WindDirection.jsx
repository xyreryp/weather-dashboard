import React from 'react';
import PropTypes from 'prop-types';
import Highcharts from "react-highcharts";
import JsonDataObj from "../../assets/data/bambey_09-2017";
import { DateTime } from 'luxon';

require('highcharts/modules/exporting')(Highcharts.Highcharts);
require('highcharts/modules/export-data')(Highcharts.Highcharts);
// require('highcharts/es-modules/modules/windbarb.src')(Highcharts.Highcharts);

class WindSpeed extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      config: [],
      data: [],
    }
  }
  render() {
    var x = [];
    var y = [];
    var wind = [];
    JsonDataObj.forEach(function(data) {
      let processed = DateTime.fromFormat(data.Date, 'yy/MM/dd HH:mm:ss');
      x.push(processed.ts);
      y.push([processed.ts, data.WindSpeed]);
      wind.push([data.WindSpeed, data.WindDirection]);
    });

    let config = {
      legend: {
        enabled: false,
      },

      chart: {
        height: 400,
        zoomType: 'xy',
      },

      exporting: {
        fileName: 'WindSpeed',
      },
      title: {
        text: "Windspeed",
      },
      subtitle: {
        text: document.ontouchstart === undefined ?
          'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
      },
      credits: {
        enabled: false,
      },
      plotOptions: {
        series: {
          type: 'windbarb',
        }
      },
      xAxis: {
        type: "datetime",
        dateTimeLabelFormats: {
          hour: '%H:%M',
          day: '%e. %b',
          week: '%e. %b',
          month: '%b \'%y',
          year: '%Y'
        }
      },
      yAxis: {
        title: {
          enabled: true,
          text: 'Windspeed',
        }

      },
      series: [

        {
          type: 'area',
          id:'wind-spped',
          data: y,
        }
      ],

      tooltip: {
        valueSuffix: ' m/s',
        useHTML: true,
      },
    };


    return (
      <div>
        <Highcharts
          config={config}
        />
      </div>
    );
  }
}

export default WindSpeed;