import React from 'react';
import PropTypes from 'prop-types';
import Highcharts from "react-highcharts";
import JsonDataObj from "../../assets/data/bambey_09-2017";
import { DateTime } from 'luxon';

class DEW extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      config: [],
      data: [],
    }
  }
  render() {
    var x = [];
    var y = [];

    JsonDataObj.forEach(function(data) {
      let processed = DateTime.fromFormat(data.Date, 'yy/MM/dd HH:mm:ss');
      y.push([processed.ts, data.Dew]);
    });

    let config = {
      legend: {
        enabled: false,
      },

      exporting: {
        showTable: false,
        fileName: 'DEW',
      },

      chart: {
        height: 400,
        zoomType: 'xy',
      },
      title: {
        text: "DEW",
      },
      subtitle: {
        text: document.ontouchstart === undefined ?
          'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
      },
      credits: {
        enabled: false,
      },

      xAxis: {

        type: "datetime",
        dateTimeLabelFormats: {
          hour: '%H:%M',
          day: '%e. %b',
          month: '%b \'%y',
          year: '%Y'
        }
      },

      yAxis: {
        title: {
          enabled: true,
          text: 'DEW',
        },

      },
      series: [{
        type: 'area',
        name: 'DEW',
        data: y,
      }],
      tooltip: {
        useHTML: true,
      },
    };



    return (
      <div>
        <Highcharts
          config={config}
        />
      </div>
    );
  }
}

export default DEW;