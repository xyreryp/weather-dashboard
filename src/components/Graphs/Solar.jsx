import React from 'react';
import PropTypes from 'prop-types';
import Highcharts from "react-highcharts";
import JsonDataObj from "../../assets/data/bambey_09-2017";
import { DateTime } from 'luxon';

class Solar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      config: [],
      data: [],
    }
  }

  // componentDidMount() {
  //
  //   this._loadGraphData();
  // }
  //
  // componentDidUpdate(prevProps, prevState) {
  //   if(this.state.config = null) {
  //     this._loadGraphData(this.props);
  //   }
  // }
  //
  _loadGraphData(props) {
  }

  render() {

    var x = [];
    var y = [];

    JsonDataObj.forEach(function(data) {
      let processed = DateTime.fromFormat(data.Date, 'yy/MM/dd HH:mm:ss');
      y.push([processed.ts, data.Solar]);
    });

    let config = {
      legend: {
        enabled: false,
      },

      exporting: {
        showTable: false,
        fileName: 'Solar',
      },

      chart: {
        height: 400,
        zoomType: 'xy',
      },
      title: {
        text: "Solar",
      },
      subtitle: {
        text: document.ontouchstart === undefined ?
          'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
      },
      credits: {
        enabled: false,
      },

      xAxis: {
        categories: x,
        type: "datetime",
        dateTimeLabelFormats: {
          hour: '%H:%M',
          day: '%e. %b',
          month: '%b \'%y',
          year: '%Y'
        }
      },

      yAxis: {
        title: {
          enabled: true,
          text: 'Solar',
        },

      },
      series: [{
        type: 'Line',
        name: 'Solar',
        data: y,
      }],
      tooltip: {
        useHTML: true,
      },
    };

    this.setState({
      config: config,
    });

    return (
      <div>
        <Highcharts
          config={config}
        />
      </div>
    );
  }
}

export default Solar;