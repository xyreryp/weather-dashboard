import React from 'react';
import PropTypes from 'prop-types';
import Highcharts from "react-highcharts";
import JsonDataObj from "../../assets/data/bambey_09-2017";
import { DateTime } from 'luxon';

class RH extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      config: [],
      data: [],
    }
  }

  // componentDidMount() {
  //
  //   this._loadGraphData();
  // }
  //
  // componentDidUpdate(prevProps, prevState) {
  //   if(this.state.config = null) {
  //     this._loadGraphData(this.props);
  //   }
  // }
  //
  // _loadGraphData(props) {
  //   var series = [];
  //   var x = [];
  //   var y = [];
  //
  //   JsonDataObj.forEach(function(data) {
  //     console.log(data.Rain);
  //     x.push(data.Date);
  //     y.push(data.Rain);
  //   });
  //   let config = {
  //     legend: {
  //       enabled: false,
  //     },
  //
  //     exporting: {
  //       showTable: false,
  //       fileName: 'Rain',
  //     },
  //
  //     chart: {
  //       height: 400,
  //       type: 'column',
  //       zoomType: 'xy',
  //     },
  //
  //     credits: {
  //       enabled: false,
  //     },
  //
  //     xAxis: {
  //       categories: x,
  //     },
  //
  //     yAxis: {
  //       data: y,
  //       title: {
  //         enabled: true,
  //         text: 'Mixed Air Temperature',
  //       },
  //     },
  //
  //     tooltip: {
  //       useHTML: true,
  //     },
  //   };
  //
  //
  //   this.setState({
  //     config: config,
  //   });
  // }

  render() {
    var x = [];
    var y = [];

    JsonDataObj.forEach(function(data) {
      let processed = DateTime.fromFormat(data.Date, 'yy/MM/dd HH:mm:ss');
      // x.push(processed.ts);
      y.push([processed.ts, data.RH]);
    });

    let config = {
      legend: {
        enabled: false,
      },

      exporting: {
        showTable: false,
        fileName: 'Rain',
      },

      chart: {
        height: 400,
        zoomType: 'xy',
      },
      title: {
        text: "Rain",
      },
      subtitle: {
        text: document.ontouchstart === undefined ?
          'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
      },
      credits: {
        enabled: false,
      },

      xAxis: {
        type: "datetime",
        dateTimeLabelFormats: {
          hour: '%H:%M',
          day: '%e. %b',
          month: '%b \'%y',
          year: '%Y'
        }
      },

      yAxis: {
        title: {
          enabled: true,
          text: 'RH',
        },

      },
      series: [{
        type: 'area',
        name: 'RH',
        data: y,
      }],
      tooltip: {
        valueSuffix: ' %',
        useHTML: true,
      },
    };


    return (
      <div>
        <Highcharts
          config={config}
        />
      </div>
    );
  }
}

export default RH;