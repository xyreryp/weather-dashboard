import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class Locations extends React.Component {
  state = {
    location: '',
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-native-helper">Station</InputLabel>
          <NativeSelect
            value={this.state.location}
            onChange={this.handleChange('location')}
            input={<Input id="age-native-helper" />}
          >
            <option value="" />
            <option value={"Bambey"}>Bambey</option>
            <option value="Kolda">Kolda</option>
            <option value="Nioro">Nioro</option>
            <option value="Sinthiou">Sinthiou</option>
          </NativeSelect>
        </FormControl>
      </div>
    );
  }
}

Locations.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Locations);